﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business {
    public partial class Person {

        public string GetCivility() {
            var db = FORMATION2Entities.FromObject(this);

            if (db == null)
                throw new Exception("Le context a probablement était disposé.");
            return db.TA_CIVILITE.Where(x => x.CIVID == this.CONTCIV).Select(z => z.CIVLIBLONG).FirstOrDefault();
        }


        public string GetLang() {
            var db = FORMATION2Entities.FromObject(this);

            if (db == null)
                throw new Exception("Le context a probablement était disposé.");
            return db.TA_LANGUE.Where(x => x.LNGID == this.CONTLANGUE).Select(x => x.LNGDES).FirstOrDefault() == default(string) ? "FR" : db.TA_LANGUE.Where(x => x.LNGID == this.CONTLANGUE).Select(x => x.LNGDES).FirstOrDefault();
        }

        public IQueryable<Session> GetStudentSession() {
            List<Session> result = new List<Session>();

            foreach (var student in this.GetFunctionStudent()) {
                var session = student.GetSession();
                if (session != default(Session))
                    result.Add(session);
            }
            return result == null ? default(IQueryable<Session>) : result.AsQueryable();
        }


        public IQueryable<Student> GetManagerStudent() {
            var db = FORMATION2Entities.FromObject(this);
            List<Student> result = new List<Student>();

            if (db == null)
                throw new Exception("Le context a probablement était disposé.");
            var trCode = db.T_TEMPLATE_ROLE.Where(x => x.TRID == (int)Enum.ePersonRole.MANAGER).Select(x => x.TRCODE).FirstOrDefault();
            var listOfFolder = db.T_DOSSIER_ROLE.Where(x => x.TRCODE.Equals(trCode) && x.CONTID == this.CONTID);
            foreach (var folder in listOfFolder) {
                var student = db.Student.Where(x => x.DOSID == folder.DOSID).FirstOrDefault();
                result.Add(student);
            }
            return result == null ? default(IQueryable<Student>) : result.AsQueryable();
        }

        public IQueryable<Session> GetManagerSession() {
            var db = FORMATION2Entities.FromObject(this);
            List<Session> result = new List<Session>();

            if (db == null)
                throw new Exception("Le context a probablement était disposé.");
            var trCode = db.T_TEMPLATE_ROLE.Where(x => x.TRID == (int)Enum.ePersonRole.MANAGER).Select(x => x.TRCODE).FirstOrDefault();
            var listOfFolder = db.T_DOSSIER_ROLE.Where(x => x.TRCODE.Equals(trCode) && x.CONTID == this.CONTID);
            foreach (var folder in listOfFolder) {
                var student = db.Student.Where(x => x.DOSID == folder.DOSID).FirstOrDefault();
                result.Add(student.GetSession());
            }
            return result == null ? default(IQueryable<Session>) : result.AsQueryable();
        }

        public bool IsInRole(Enum.ePersonRole role) {
            var db = FORMATION2Entities.FromObject(this);

            if (db == null)
                throw new Exception("Le context a probablement était disposé.");
            switch (role) {
                case Enum.ePersonRole.STUDENT:
                    return db.Student.Where(x => x.CONTID == this.CONTID).Count() > 0;
                case Enum.ePersonRole.BILLING:
                case Enum.ePersonRole.MANAGER:
                case Enum.ePersonRole.OUTSOURCER:
                case Enum.ePersonRole.SUPERVISOR:
                    var trCode = db.T_TEMPLATE_ROLE.Where(x => x.TRID == (int)role).Select(x => x.TRCODE).FirstOrDefault();
                    return db.T_DOSSIER_ROLE.Where(x => x.TRCODE.Equals(trCode) && x.CONTID == this.CONTID).Count() > 0;
                default:
                    throw new ArgumentException("Le rôle n'existe pas.");
            }
        }

        public IQueryable<Student> GetFunctionStudent() {
            List<Student> result = new List<Student>();
            var db = FORMATION2Entities.FromObject(this);

            if (db == null)
                throw new Exception("Le context a probablement était disposé.");

            var listOfParticipation = from e in db.Student
                                      where e.CONTID == this.CONTID
                                      select e;

            foreach (var pax in listOfParticipation)
                result.Add(pax);
            return result == null ? default(IQueryable<Student>) : result.AsQueryable();

        }

    }
}
