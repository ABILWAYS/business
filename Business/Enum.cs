﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business {
    public class Enum {

        public enum eBrand {
            CFPJ_MEDIAS = 1,
            CFPJ_COM = 2,
            ACP = 3,
            EFE = 4,
            ISM = 424,
            IFE_BELGIQUE = 555,
            IFE_LUX = 556,
            IDCC = 999,
            PYRAMYD = 1154,
            IFE_PORTUGAL = 1300
        }


        public enum ePersonRole {
            STUDENT = 0,
            // Donneur d'ordre
            OUTSOURCER = 1,
            BILLING = 2,
            OTHER = 3,
            SUPERVISOR = 4,
            MANAGER = 5
        

        }

        public enum eModuleDeliveryType {
            VIRTUAL_CLASSROOM,
            ON_THE_SPOT
        }

        public enum eParticipationWorkShop {
            UNDEFINIED = 0,
            PRESENT = 1,
            ABSENT = 2,
            ABSENT_EXCUSE = 3
        }

        public enum eStatus {
            OUVERT = 1,
            COMPLET = 2,
            ANNULEE = 3,
            TO_FACTURE = 7,
            REPORTEE = 8,
            CLOTUREE = 9,
            BLOCAGE = 10
        }
    }
}
