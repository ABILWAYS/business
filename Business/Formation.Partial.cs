﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Business.Enum;

namespace Business
{
    public partial class Formation {

        [System.Data.Entity.DbFunction("FORMATION2Model.store", "Statut_Cycle")]
        public static string Statut_Cycle(Int32 CYC_id) {
            throw new NotSupportedException("Les appels direct sur cette fonction ne sont pas suportés");
        }

        #region Formation

        public eBrand GetBrand() {
            var db = FORMATION2Entities.FromObject(this);

            if (db == null)
                throw new Exception("Le context a probablement était disposé.");
            return (eBrand)db.PRO_produit.Where(x => x.PRO_id == this.PRO_id).Select(x => x.RUB_id).FirstOrDefault();
        }

        public Boolean IsBlended() {
            throw new NotImplementedException();
        }

        #endregion

        #region Session

        /// <summary>
        /// Obtenir la liste des sessions associées.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Session> GetSession() {
            return this.CYC_cycle.AsQueryable().Cast<Session>();
        }

        /// <summary>
        /// Obtenir la liste des sessions non-annulées.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Session> GetValidSession() {
            List<Session> result = new List<Session>();

            using (var db = new FORMATION2Entities()) {
                foreach (var cyc in this.CYC_cycle) {
                    if ((eStatus)Int32.Parse(Statut_Cycle((int)cyc.CYC_id)) != eStatus.ANNULEE
                        && (eStatus)Int32.Parse(Statut_Cycle((int)cyc.CYC_id)) != eStatus.REPORTEE) {
                        result.Add((cyc));
                    }
                }
                return result == null ? null : result.AsQueryable().Cast<Session>();
            }
        }

        /// <summary>
        /// Obtenir la liste des sessions annulées.
        /// </summary>
        /// <returns>Null ou l'ensemble des sessions</returns>
        public IQueryable<Session> GetCanceledSession() {
            List<Session> result = new List<Session>();

            using (var db = new FORMATION2Entities()) {
                foreach (var cyc in this.CYC_cycle) {
                    if ((eStatus)Int32.Parse(Statut_Cycle((int)cyc.CYC_id)) == eStatus.ANNULEE
                        || (eStatus)Int32.Parse(Statut_Cycle((int)cyc.CYC_id)) == eStatus.REPORTEE) {
                        result.Add(cyc);
                    }
                }
                return result == null ? null : result.AsQueryable().Cast<Session>();
            }
        }

        #endregion

        #region Contributor

        public IQueryable<Contributor> GetContributor() {
            throw new NotImplementedException();
        }


        #endregion

        #region Student


        /// <summary>
        /// Retourne la liste de tous les participants de la formation
        /// </summary>
        /// <returns>La liste des participants</returns>
        public IQueryable<Student> GetStudent() {
            IQueryable<Student> result = default(IQueryable<Student>);

            foreach (var session in GetSession()) {
                if (result == default(IQueryable<Student>))
                    result = session.GetStudent();
                else
                result = result.Concat(session.GetStudent());
            }
            return result == default(IQueryable<Student>) ? result : result.Distinct();
        }


        /// <summary>
        /// Check si un particianpant est inscrit sur une session valide
        /// </summary>
        /// <param name="pax">Participant</param>
        /// <returns>Bool</returns>
        public bool IsStudentRegistered(Student pax) {
            foreach (var ses in GetValidSession())
                if (ses.GetStudent().Contains(pax))
                    return true;
            return false;
        }

        #endregion

    }
}
