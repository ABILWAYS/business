﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business {
    public partial class Student {

        /// <summary>
        /// Vérifie le status du participant dans e-basicc
        /// </summary>
        /// <param name="status">TA_STATUS_PARTICIPANT id</param>
        /// <returns>Bool</returns>
        private bool IsStudentRegistered(int status) {
            var db = FORMATION2Entities.FromObject(this);

            if (db == null)
                throw new Exception("Le context a probablement été fermé.");
            var partStat = from stat in db.TA_STAT_PARTICIPANT
                           where stat.STATPARTID == status
                           select stat.STATGROUPE;
            if ("Inscrit".Equals(partStat.First()))
                return true;
            return false;
        }


        public Formation GetFormation() {
            return GetSession() == null ? null : GetSession().GetFormation();
        }

        public Session GetSession() {
            var db = FORMATION2Entities.FromObject(this);

            if (db == null)
                throw new Exception("Le context a probablement été fermé.");

            if (IsStudentRegistered(this.PARTSTAT.Value)) {
                var sessionID = db.T_PARTICIPANTS_CYCLE.Where(x => x.PARTID == this.PARTID).Select(x => x.CYC_id).FirstOrDefault();
                if (!sessionID.HasValue)
                    return default(Session);
                return db.Session.Where(x => x.CYC_id == sessionID).FirstOrDefault();
            }
            return null;
        }

        public Person GetPerson() {
            return this.T_CONTACT;
        }

    }
}
