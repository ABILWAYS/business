﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Business.Enum;

namespace Business {
    public partial class WorkShop {


        public double GetDuration() {
            return this.SPE_dureeHeure;
        }

        #region Student

        public bool IsStudentRegistered(Student pax) {
            return GetStudent().Contains(pax);
        }


        public IQueryable<Student> GetStudent() {
            List<Student> result = new List<Student>();
            var db = FORMATION2Entities.FromObject(this);

            if (db == null)
                throw new Exception("Le context a probablement été fermé.");
            foreach (var pax in db.T_PARTICIPANTS_SESSION.Where(x => x.SESSJID == this.SPE_id && x.PARTSSEL == "O").Select(x => x.T_PARTICIPANTS)) {
                result.Add(pax);
            }
            return result == null ? default(IQueryable<Student>) : result.AsQueryable().Cast<Student>();
        }

        public IQueryable<Student> GetStudent(FORMATION2Entities db) {
            List<Student> result = new List<Student>();

            foreach (var pax in db.T_PARTICIPANTS_SESSION.Where(x => x.SESSJID == this.SPE_id).Select(x => x.T_PARTICIPANTS)) {
                result.Add(pax);
            }
            return result == null ? default(IQueryable<Student>) : result.AsQueryable().Cast<Student>();
        }

        public IQueryable<Student> GetValidStudent() {
            List<Student> result = new List<Student>();

            using (var db = new FORMATION2Entities()) {
                foreach (var pax in db.T_PARTICIPANTS_SESSION.Where(x => x.SESSJID == this.SPE_id).Select(x => x.T_PARTICIPANTS)) {
                    result.Add(pax);
                }
            }
            return result == null ? default(IQueryable<Student>) : result.AsQueryable().Cast<Student>();
        }
        #endregion

        public Double GetPresenceDuration(Student pax) {
            if (!GetStudent().Contains(pax)) {
                throw new Exception("Le participant n'est pas inscrit");
            }
            else {
                using (var db = new FORMATION2Entities()) {
                    var participation = db.T_PARTICIPANTS_SESSION.Where(x => x.SESSJID == this.SPE_id && x.PARTID == pax.PARTID).Select(x => x.PARTSDUREE).FirstOrDefault();
                    if (participation.HasValue)
                        return participation.Value;
                    return default(double);
                }
            }
        }

        public bool IsStudentPresent(Student pax) {
            if (!GetStudent().Contains(pax)) {
                throw new Exception("Le participant n'est pas inscrit");
            }
            else {
                using (var db = new FORMATION2Entities()) {
                    var participation = (eParticipationWorkShop)db.T_PARTICIPANTS_SESSION.Where(x => x.SESSJID == this.SPE_id && x.PARTID == pax.PARTID).Select(x => x.PARTSSTAT).FirstOrDefault();
                    switch (participation) {
                        case eParticipationWorkShop.PRESENT:
                            return true;
                        default:
                            return false;
                    }
                }
            }
        }
    }
}
