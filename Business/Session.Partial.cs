﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Business.Enum;

namespace Business {
    public partial class Session {

        [System.Data.Entity.DbFunction("FORMATION2Model.store", "Statut_Session")]
        public static string Statut_Session(Int32 SES_id) {
            throw new NotSupportedException("Les appels direct sur cette fonction ne sont pas suportés");
        }

        [System.Data.Entity.DbFunction("FORMATION2Model.store", "Statut_Cycle")]
        public static string Statut_Cycle(Int32 CYC_id) {
            throw new NotSupportedException("Les appels direct sur cette fonction ne sont pas suportés");
        }

        #region Session

        public bool IsActive() {
            if ((eStatus)Int32.Parse(Statut_Cycle((int)this.CYC_id)) != eStatus.ANNULEE
                       && (eStatus)Int32.Parse(Statut_Cycle((int)this.CYC_id)) != eStatus.REPORTEE) {
                return true;
            }
            return false;
        }

        public eStatus GetStatus() {
            return (eStatus)Int32.Parse(Statut_Cycle((int)this.CYC_id));
        }


        public bool IsGuarentee() {
            return this.CYC_garanti;
        }

        #endregion

        #region Formation

        public Formation GetFormation() {
            return this.FOR_formation;
        }

        #endregion

        #region Student

        public bool IsStudentRegistered(Student pax) {
            return GetStudent().Contains(pax);
        }

        public IQueryable<Student> GetStudent() {
            IQueryable<Student> result = default(IQueryable<Student>);

            foreach (var module in GetModule()) {
                if (result == default(IQueryable<Student>))
                    result = module.GetStudent();
                else
                    result = result.Concat(module.GetStudent());
            }
            return result == default(IQueryable<Student>) ? result : result.Distinct();
        }

        public IQueryable<Student> GetValidStudent() {
            throw new NotImplementedException();
        }

        public IQueryable<Student> GetCanceledStudent() {
            throw new NotImplementedException();
        }

        #endregion

        #region Contributor
        /// <summary>
        /// Retourne la liste des intervenants de tous les modules.
        /// </summary>
        /// <returns>Default ou Iquerybale<Contributor></returns>
        public IQueryable<Contributor> GetContributor() {
            IQueryable<Contributor> result = default(IQueryable<Contributor>);

            foreach (var module in GetModule())
                result = result.Concat(module.GetContributor());
            return result == default(IQueryable<Contributor>) ? result : result.Distinct();
        }

        /// <summary>
        /// Retourne la liste des intervenants valide de tous les modules
        /// </summary>
        /// <returns>Default ou Iquerybale<Contributor></returns>
        public IQueryable<Contributor> GetValidContributor() {
            IQueryable<Contributor> result = default(IQueryable<Contributor>);

            foreach (var module in GetModule())
                result = result.Concat(module.GetValidContributor());
            return result == default(IQueryable<Contributor>) ? result : result.Distinct();
        }

        /// <summary>
        /// Retourne la liste des intervenants annulés de tous les modules
        /// </summary>
        /// <returns>Default ou Iquerybale<Contributor></returns>
        public IQueryable<Contributor> GetCanceledContributor() {
            IQueryable<Contributor> result = default(IQueryable<Contributor>);

            foreach (var module in GetModule())
                result = result.Concat(module.GetCanceledContributor());
            return result == default(IQueryable<Contributor>) ? result : result.Distinct();
        }

        #endregion

        #region Module

        public IQueryable<Module> GetModule() {
            List<Module> result = new List<Module>();


            foreach(var ces in this.CYC_SES_assembler) {
                result.Add(ces.SES_session);
            }
            return result == null ? default(IQueryable<Module>) : result.AsQueryable().Cast<Module>();
        }

        public IQueryable<Module> GetValidModule() {
            List<Module> result = new List<Module>();

            using (var db = new FORMATION2Entities()) {
                foreach (var ses in db.CYC_SES_assembler.Where(x => x.CYC_id == this.CYC_id).Select(x => x.SES_session)) {
                    if ((eStatus)Int32.Parse(Statut_Session((int)ses.SES_id)) != eStatus.ANNULEE
                        && (eStatus)Int32.Parse(Statut_Session((int)ses.SES_id)) != eStatus.REPORTEE)
                        result.Add(ses);
                }
            }
            return result == null ? null : result.AsQueryable().Cast<Module>();
        }

        public IQueryable<Module> GetCanceledModule() {
            List<Module> result = new List<Module>();

            using (var db = new FORMATION2Entities()) {
                foreach (var ses in db.CYC_SES_assembler.Where(x => x.CYC_id == this.CYC_id).Select(x => x.SES_session)) {
                    if ((eStatus)Int32.Parse(Statut_Session((int)ses.SES_id)) == eStatus.ANNULEE
                        || (eStatus)Int32.Parse(Statut_Session((int)ses.SES_id)) == eStatus.REPORTEE)
                        result.Add(ses);
                }
            }
            return result == null ? null : result.AsQueryable().Cast<Module>();
        }

        #endregion

    }
}
