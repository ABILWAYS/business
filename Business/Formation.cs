//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class Formation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Formation()
        {
            this.CYC_cycle = new HashSet<Session>();
        }
    
        public long FOR_id { get; set; }
        public long PRO_id { get; set; }
        public string FOR_codeFormation { get; set; }
        public string FOR_titre { get; set; }
        public string FOR_titreWeb { get; set; }
        public string FOR_sousTitre { get; set; }
        public string FOR_sousTitreWeb { get; set; }
        public string FOR_objectifs { get; set; }
        public string FOR_objectifsWeb { get; set; }
        public string FOR_prerequis { get; set; }
        public string FOR_prerequisWeb { get; set; }
        public string FOR_contexte { get; set; }
        public string FOR_contexteWeb { get; set; }
        public string FOR_publicConcerne { get; set; }
        public string FOR_publicConcerneWeb { get; set; }
        public string FOR_type { get; set; }
        public Nullable<bool> FOR_atelier { get; set; }
        public string FOR_url { get; set; }
        public string FOR_certificat { get; set; }
        public string FOR_modaliteInscription { get; set; }
        public Nullable<bool> FOR_nouveau { get; set; }
        public Nullable<double> FOR_taille { get; set; }
        public string FOR_introduction { get; set; }
        public string FOR_introductionWeb { get; set; }
        public Nullable<double> FOR_page { get; set; }
        public Nullable<bool> FOR_repasCompris { get; set; }
        public string FOR_postFormation { get; set; }
        public string FOR_anglais { get; set; }
        public string FOR_calculatrice { get; set; }
        public string FOR_approchePedagogique { get; set; }
        public string FOR_autreCodeFormation { get; set; }
        public Nullable<bool> FOR_vitrine { get; set; }
        public string FOR_acquisFormation { get; set; }
        public string FOR_duree { get; set; }
        public double FOR_dureeHeure { get; set; }
        public double FOR_dureeJour { get; set; }
        public Nullable<bool> FOR_modifiee { get; set; }
        public string FOR_intervenant { get; set; }
        public string FOR_diplome { get; set; }
        public string FOR_moyensTechniques { get; set; }
        public string FOR_animation { get; set; }
        public string FOR_evaluation { get; set; }
        public string FOR_niveau { get; set; }
        public string FOR_expertisePlus { get; set; }
        public string FOR_competenceMetier { get; set; }
        public string FOR_wifi { get; set; }
        public string FOR_format { get; set; }
        public Nullable<bool> FOR_happyHour { get; set; }
        public Nullable<System.DateTime> FOR_dateUploadPdf { get; set; }
        public bool FOR_possedePdf { get; set; }
        public string FOR_langue { get; set; }
        public Nullable<double> FOR_ca { get; set; }
        public string FOR_codeMarketing { get; set; }
        public Nullable<int> TPRODCPTAID { get; set; }
        public string FOR_division { get; set; }
        public string FOR_codeOrigine { get; set; }
        public string FOR_formatCategorie { get; set; }
        public Nullable<long> FOR_idTO { get; set; }
        public Nullable<System.DateTime> FOR_lastUpdateTO { get; set; }
        public string FOR_codeOrigine2 { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Session> CYC_cycle { get; set; }
        public virtual PRO_produit PRO_produit { get; set; }
    }
}
